@extends('layout.master')

@section('judul')
    Halaman Detail Film
@endsection

@section('content')

<div class="row">


    <div class="col-12">
        <div class="card">
            <img src="{{asset('gambar/'.$film->poster)}}" width="300px" class="p-2">
            <div class="card-body">
              <h5 class="card-title"><strong>{{$film->judul}}</strong></h5>
              <p class="card-text">{{$film->ringkasan}}</p>
              <a href="/film" class="btn btn-primary">Kembali</a>
            </div>
          </div>
    </div> 

</div>

@endsection