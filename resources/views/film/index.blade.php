@extends('layout.master')

@section('judul')
    Halaman List Film
@endsection

@section('content')

<a href="/film/create" class="btn btn-primary btn-sm my-2">Tambah Film</a>

<div class="row">

    @forelse ($film as $item)
    <div class="col-12">
        <div class="card">
            <img src="{{asset('gambar/'.$item->poster)}}" width="250px" class="p-2">
            <div class="card-body">
              <h5 class="card-title"><strong>{{$item->judul}}</strong></h5>
              <p class="card-text">{{ Str::limit($item->ringkasan, 72)}}</p>
              <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
            </div>
          </div>
    </div> 
    @empty
        <h1>Tidak Ada Film</h1>
    @endforelse


</div>
@endsection