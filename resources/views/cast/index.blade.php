@extends('layout.master')

@section('judul')
    Halaman Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Cast</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Pemeran</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
        @empty
        <tr>
            <td>
                <h1>Data Kosong</h1>
            </td>
        </tr>
        @endforelse
    </tbody>
  </table>
@endsection