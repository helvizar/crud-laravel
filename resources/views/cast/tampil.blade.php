@extends('layout.master')

@section('judul')
    Halaman Detail Cast
@endsection

@section('content')
    <h1>{{$cast->name}}</h1>
    <p>Umur : {{$cast->umur}}</p>
    <p>Peran : {{$cast->bio}}</p>
@endsection