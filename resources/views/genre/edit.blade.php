@extends('layout.master')

@section('judul')
    Halaman Edit Genre
@endsection

@section('content')
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Genre</label>
      <input type="text" value="{{$genre->name}}" name="name" class="form-control">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection