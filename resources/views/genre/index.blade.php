@extends('layout.master')

@section('judul')
    Halaman Genre
@endsection

@section('content')

<a href="/genre/create" class="btn btn-primary btn-sm my-2">Tambah Genre</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Genre</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>
                    <form action="/genre/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/genre/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
        @empty
        <tr>
            <td>
                <h1>Data Kosong</h1>
            </td>
        </tr>
        @endforelse
    </tbody>
  </table>
@endsection