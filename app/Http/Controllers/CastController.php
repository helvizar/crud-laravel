<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Validated;
use Illuminate\Foundation\Console\Presets\React;
use Illuminate\Http\Request;
//fungsi db
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function simpan(Request $request)
    {
        // dd($request->all());
        //validasi
        $request->validate(
            [
                'name' => 'required',
                'umur' => 'required',
                'bio' => 'required'
            ],
            [
                'name.required' => 'Kolom Nama Tidak Boleh Kosong',
                'umur.required' => 'Kolom Umur Tidak Boleh Kosong',
                'umur.required' => 'Kolom Bio Tidak Boleh Kosong'
            ]
        );

        DB::table('cast')->insert(
            [
                'name' => $request['name'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        // dd($cast);
        return view('cast.index', compact('cast'));
    }

    public function tampil($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.tampil', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        //validasi
        $request->validate(
            [
                'name' => 'required',
                'umur' => 'required',
                'bio' => 'required'
            ],
            [
                'name.required' => 'Kolom Nama Tidak Boleh Kosong',
                'umur.required' => 'Kolom Umur Tidak Boleh Kosong',
                'umur.required' => 'Kolom Bio Tidak Boleh Kosong'
            ]
        );

        $cast = DB::table('cast')
                ->where('id', $id)
                ->update(
                    [
                        'name' => $request['name'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']
                    ]
                );
            return redirect('/cast');
    }

    public function delete($id)
    {
        Db::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
