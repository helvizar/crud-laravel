<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Validated;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;


class GenreController extends Controller
{
    public function create()
    {
        return view('genre.create');
    }

    public function simpan(Request $request)
    {
        $request->validate(
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Kolom Genre Tidak Boleh Kosong'
            ]
        );

        DB::table('genre')->insert(
            [
                'name' => $request['name']
            ]
        );

        return redirect('/genre');
    }

    public function index()
    {
        $genre = DB::table('genre')->get();
        
        return view('genre.index', compact('genre'));
    }

    public function tampil($id)
    {
        $genre = DB::table('genre')->where('id', $id)->first();

        return view('genre.tampil', compact('genre'));
    }

    public function edit($id)
    {
        $genre = DB::table('genre')->where('id', $id)->first();

        return view('genre.edit', compact('genre'));
    }

    public function update($id, Request $request)
    {
               //validasi
               $request->validate(
                [
                    'name' => 'required'
                ],
                [
                    'name.required' => 'Kolom Genre Tidak Boleh Kosong'
                ]
            );
    
            $genre = DB::table('genre')
                    ->where('id', $id)
                    ->update(
                        [
                            'name' => $request['name']
                        ]
                    );
                return redirect('/genre');
    }
}
