<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;


Route::get('/', function () {
    return view('home');
});


//CRUD cast

//Create
//Form Input Data Create Cast
Route::get('/cast/create', 'CastController@create');

//untuk menyimpan data ke database
Route::post('/cast', 'CastController@simpan');

//Read
//Menampilkan semua data di table cast
Route::get('/cast', 'CastController@index');

//menampilkan detail cast berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@tampil');

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

//update data berdasarkan id di tabel cast
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete
Route::delete('/cast/{cast_id}', 'CastController@delete');

// CRUD Genre
Route::get('/genre/create', 'GenreController@create');

Route::post('/genre', 'GenreController@simpan');

Route::get('/genre', 'GenreController@index');

Route::get('/genre/{genre_id}', 'GenreController@tampil');

Route::get('/genre/{genre_id}/edit', 'GenreController@edit');

Route::put('/genre/{genre_id}', 'GenreController@update');


// CRUD FILM
Route::resource('film', 'FilmController');




